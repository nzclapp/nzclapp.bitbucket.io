var classFTerm__IMUdriver_1_1BNO055 =
[
    [ "__init__", "classFTerm__IMUdriver_1_1BNO055.html#a90c934330aac8292d0c7b2d79b8b1a5f", null ],
    [ "get_cal_coeff", "classFTerm__IMUdriver_1_1BNO055.html#a342a51cd99ebb703e3a6078bf875dd8e", null ],
    [ "get_cal_status", "classFTerm__IMUdriver_1_1BNO055.html#a122a20deabae00b3813c9eccb56bb50a", null ],
    [ "OPmode", "classFTerm__IMUdriver_1_1BNO055.html#a935519e88adce61d8d5e6eedeba05d5c", null ],
    [ "read_angles", "classFTerm__IMUdriver_1_1BNO055.html#a134a9bbcebb867c64fd995d9dc5b1f2a", null ],
    [ "read_angularvelo", "classFTerm__IMUdriver_1_1BNO055.html#ace516a89e26cf2b787fe2afe3bb3a789", null ],
    [ "set_cal_coeff", "classFTerm__IMUdriver_1_1BNO055.html#a728bc9e8745eaf1e9d48b14af57b123b", null ],
    [ "measuredx", "classFTerm__IMUdriver_1_1BNO055.html#a3feac337c002304bfa667144749a9163", null ],
    [ "measuredy", "classFTerm__IMUdriver_1_1BNO055.html#aa776267b2a5b7496c57361f0b1adc0a1", null ]
];