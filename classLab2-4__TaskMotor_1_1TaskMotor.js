var classLab2_4__TaskMotor_1_1TaskMotor =
[
    [ "__init__", "classLab2-4__TaskMotor_1_1TaskMotor.html#a529d4bfd3a377b1dc37c1bcc960fc229", null ],
    [ "run", "classLab2-4__TaskMotor_1_1TaskMotor.html#a53c0a8ff8938a2191c963614c5ff9c3c", null ],
    [ "a", "classLab2-4__TaskMotor_1_1TaskMotor.html#a131b821c4d2468d01910601cc268e70b", null ],
    [ "CLOL", "classLab2-4__TaskMotor_1_1TaskMotor.html#abd0f9325f0e5fe83f0eabda1c2c47adb", null ],
    [ "ClosedLoopObj", "classLab2-4__TaskMotor_1_1TaskMotor.html#a5eb938ca4e073bb770485cf48f1ec6db", null ],
    [ "count", "classLab2-4__TaskMotor_1_1TaskMotor.html#a27ab919a4e322bd83493a5e40550d4eb", null ],
    [ "duty", "classLab2-4__TaskMotor_1_1TaskMotor.html#a6bb59583cf577adecf44b17123dda6c1", null ],
    [ "insignal", "classLab2-4__TaskMotor_1_1TaskMotor.html#a31dbc95d168fbaa24f034d377ed5da3f", null ],
    [ "measured", "classLab2-4__TaskMotor_1_1TaskMotor.html#a667acff91273419616dd8990d669e2e6", null ],
    [ "motordrive", "classLab2-4__TaskMotor_1_1TaskMotor.html#a5d8a39bd3d4bb5fbe39c15fbc1c7f064", null ],
    [ "MotorNum", "classLab2-4__TaskMotor_1_1TaskMotor.html#a8982bb91471e5fae290cccf11bd4fb3a", null ],
    [ "MotorObj", "classLab2-4__TaskMotor_1_1TaskMotor.html#a05c6deb63fad185c5db0698e776f2dda", null ],
    [ "period", "classLab2-4__TaskMotor_1_1TaskMotor.html#adfedd94923643e8305229af11a6bdd43", null ],
    [ "S0_Init", "classLab2-4__TaskMotor_1_1TaskMotor.html#adc999a20aae4386486c6d9c96b229071", null ],
    [ "S1_InputState", "classLab2-4__TaskMotor_1_1TaskMotor.html#a5fd712ed65a0a3cfbe48e8402b8fb55f", null ],
    [ "S2_RunCL", "classLab2-4__TaskMotor_1_1TaskMotor.html#afd6263d4c73b8fe3a34f89b732574054", null ],
    [ "S3_RunOL", "classLab2-4__TaskMotor_1_1TaskMotor.html#a6d32e85b428141b401c24ab142899a57", null ],
    [ "state", "classLab2-4__TaskMotor_1_1TaskMotor.html#a0958d9ef93a49af55a26dc20e68ee676", null ]
];