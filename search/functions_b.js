var searchData=
[
  ['sawtooth_0',['SAWTOOTH',['../Lab1__main_8py.html#a3e39a496261176c56d3f3d8c24a1239a',1,'Lab1_main']]],
  ['set_5fcal_5fcoeff_1',['set_cal_coeff',['../classFTerm__IMUdriver_1_1BNO055.html#a728bc9e8745eaf1e9d48b14af57b123b',1,'FTerm_IMUdriver::BNO055']]],
  ['set_5fduty_2',['set_duty',['../classFTerm__MotorDriver_1_1Motor.html#a5c15cbdaa65bfc2d512dec9da5f20767',1,'FTerm_MotorDriver.Motor.set_duty()'],['../classLab2-4__MotorDriver_1_1Motor.html#aa700db50f2edbccd3cefb0bf5ec1203e',1,'Lab2-4_MotorDriver.Motor.set_duty()']]],
  ['set_5fkp_3',['set_kp',['../classLab4__closedloop_1_1closedloop.html#a114c41ac85fa097a68422f74ea89ada2',1,'Lab4_closedloop::closedloop']]],
  ['set_5fposition_4',['set_position',['../classLab2-4__EncoderDriver_1_1EncoderDriver.html#a4cbe6a3422d962a8c9268f0f912cf821',1,'Lab2-4_EncoderDriver::EncoderDriver']]],
  ['sine_5',['SINE',['../Lab1__main_8py.html#afae448a9d3deb9e258d22e5d236431c7',1,'Lab1_main']]],
  ['square_6',['SQUARE',['../Lab1__main_8py.html#ae06457da88cf1afb15006098a122e8a8',1,'Lab1_main']]]
];
